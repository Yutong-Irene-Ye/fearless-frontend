https://gitlab.com/Yutong-Irene-Ye/fearless-frontend


Need to run GHI React -Attendee Server:
```python
docker run -it -w /app -v "$(pwd)/ghi:/app" node:latest npx create-react-app attendees
```


Need to add this part: 
```python
react-attendees:
  image: node:latest
  command: npm start
  working_dir: /app
  volumes:
    - ./ghi/attendees:/app
  environment:
    - HOST=0.0.0.0
    - PORT=3001
  ports:
    - "3001:3001"
```

instead of using 'class', React uses 'className', because all of that JSX HTML-looking stuff gets turned into JavaScript.

The App.js
"No. The App.js file is where we write the component. We need to pass data in from the top level. That's in the index.js file."

in the api-view.py, add the get_extra data will give you the conferences
```python
class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]

    def get_extra_data(self, o):
        return {"conference": o.conference.name}
        
```

also added
```python
        if conference_vo_id is not None:
            attendees = Attendee.objects.filter(conference=conference_vo_id)
            ```