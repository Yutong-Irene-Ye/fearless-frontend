import React, { useEffect, useState } from 'react';

function LocationForm(props) {
    // Set the useState hook to store "name" in the component's state,
  // with a default initial value of an empty string.
    const [name, setName] = useState('');
    const [roomCount, setRoomCount] = useState('');
    const [city, setCity] = useState('');
    const [selectedState, setSelectedState] = useState('');
    const [states, setStates] = useState([]); // Assuming you have a state for the list of states
        // Add state variables for city and state

      // Create the handleNameChange method to take what the user inputs
  // into the form and store it in the state's "name" variable.
    const handleNameChange = (event) => {
        setName(event.target.value);
    };

    const handleRoomCountChange = (event) => {
        setRoomCount(event.target.value);
    };

    const handleCityChange = (event) => {
        setCity(event.target.value);
    };

    const handleStateChange = (event) => {
        setSelectedState(event.target.value);
    };

    // fetchData function to fetch the list of states
     // fetchData, useEffect, and return methods...
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setStates(data.states); // Update the states state
        }
    }

    // useEffect hook to call fetchData when the component mounts
    useEffect(() => {
        fetchData();
    }, []);



    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            name: name,
            room_count: roomCount,
            city: city,
            state: selectedState
        };

        console.log(data)

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            // Resetting the form fields
            setName('');
            setRoomCount('');
            setCity('');
            setSelectedState('');
        }
    }



      
    // JSX for the LocationForm component
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new location</h1>
                    <form onSubmit={handleSubmit} id="create-location-form"></form>
                    <form id="create-location-form">
                     <div className="form-floating mb-3">
                     <input 
                              onChange={handleNameChange} 
                              placeholder="Name" 
                              required 
                              type="text" 
                              name="name" 
                              id="name" 
                              className="form-control" 
                              value={name}
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input 
                              onChange={handleRoomCountChange} 
                              placeholder="Room count" 
                              required 
                              type="number" 
                              name="room_count" 
                              id="room_count" 
                              className="form-control" 
                              value={roomCount}
                            />
                            <label htmlFor="room_count">Room count</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input 
                              onChange={handleCityChange} 
                              placeholder="City" 
                              required 
                              type="text" 
                              name="city" 
                              id="city" 
                              className="form-control" 
                              value={city}
                            />
                            <label htmlFor="city">City</label>
                        </div>
                        <div className="mb-3">
                            <select 
                              onChange={handleStateChange} 
                              required 
                              name="state" 
                              id="state" 
                              className="form-select"
                              value={selectedState}
                            >
                                <option value="">Choose a state</option>
                                {states.map(state => (
                                    <option key={state.abbreviation} value={state.abbreviation}>
                                        {state.name}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default LocationForm;
