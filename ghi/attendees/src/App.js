import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import './App.css';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm'; 
import AttendConferenceForm from './AttendConferenceForm'; // Import if exists
import MainPage from './MainPage'; // Import MainPage


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
          <Routes>
            <Route index element={<MainPage />} />
            <Route path="conferences/new" element={<ConferenceForm />} />
            <Route path="attendees/new" element={<AttendConferenceForm />} />
            <Route path="locations/new" element={<LocationForm />} />
            <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
          </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;