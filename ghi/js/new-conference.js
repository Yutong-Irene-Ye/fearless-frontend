window.addEventListener('DOMContentLoaded', async () => {
    // Load available locations
    const locationsUrl = 'http://localhost:8000/api/locations/';
    const locationsResponse = await fetch(locationsUrl);
    const locationsData = await locationsResponse.json();
    const locationSelect = document.getElementById('location');

    locationsData.locations.forEach(location => {
        const option = document.createElement('option');
        option.value = location.id;
        option.innerHTML = location.name; // or any other property you want to display
        locationSelect.appendChild(option);
    });

    // Form submission handling
    const formTag = document.getElementById('create-conference-form');
    const selectTag = document.getElementById('conference');
    console.log(selectTag)

    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);

 

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'POST',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log('Conference created:', newConference);
            // Maybe redirect to the conference list or show a success message
        }
    });
});
