window.addEventListener('DOMContentLoaded', async () => {
    // Fetch conferences and populate the select dropdown
    const conferencesUrl = 'http://localhost:8000/api/conferences/';
    const response = await fetch(conferencesUrl);

    if (response.ok) {
      const data = await response.json();
      const selectTag = document.getElementById('conference');

      for (let conference of data.conferences) {

          // Create an 'option' element
          const option = document.createElement('option');

          // Set the '.value' property of the option element to the state's abbreviation
          option.value = conference.id;

          // Set the '.innerHTML' property of the option element to the state's name
          option.innerHTML = conference.name;

          // Append the option element as a child of the select tag
          selectTag.appendChild(option);
      }
  } 

    // Form submission handling for creating a new presentation
    const formTag = document.getElementById('create-presentation-form');
    const selectTag = document.getElementById('conference')
   

    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        console.log('need to submit the form data for a new presentation');

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        

        const conferenceId = selectTag.options[selectTag.selectedIndex].value
        console.log('conferenceId', conferenceId)

        const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
        const fetchConfig = {
          method: "post",
          body: json,
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const postResponse = await fetch(presentationUrl, fetchConfig);
        if (postResponse.ok) {
          formTag.reset();
          const newPresentation = await postResponse.json();
          console.log(newPresentation);
        }
    });
});
